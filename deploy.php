<?php
namespace Deployer;

include_once __DIR__ . '/vendor/autoload.php';
include_once __DIR__ . '/vendor/deployer/deployer/recipe/composer.php';

host('staging')
    ->hostname('jizo.oderland.com')
    ->port(22)
    ->set('deploy_path', '/home/sultande/public_html/teatergatan')
    ->user('sultande')
    ->set('branch', 'master')
    ->stage('staging')
    ->identityFile('/Users/Henrik/Webb/nycklar/oderland/id_rsa');

host('production')
        ->hostname('bandol.oderland.com')
        ->port(22)
        ->set('deploy_path', '/home/valandse/public_html/teatergatan')
        ->user('valandse')
        ->set('branch', 'master')
        ->stage('production')
        ->identityFile('/Users/Henrik/Webb/nycklar/oderland/valandse');

set('repository', 'git@bitbucket.org:HenrikKamstedt/teatergatan.git');

set('env_vars', '/usr/bin/env');
set('keep_releases', 5);
set('shared_dirs', ['web/app/uploads']);
set('shared_files', ['.env']);
