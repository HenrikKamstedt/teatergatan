<?php 
	$today = date('Ymd');

                    $args = array (
    					'post_type' 	=> 'event',
    					'meta_key'		=> 'eventdate',
    					'orderby' 		=> 'meta_value',
						'order' 		=> 'ASC',
						'posts_per_page' => 5,
    					'meta_query' 	=> array(
	        				'key'		=> 'eventdate',
	        				'compare'	=> '>=',
	        				'value'		=> $today,
						),
					);

// get posts
$loop = new WP_Query( $args );
?>
				<?php while( $loop->have_posts() ) : $loop->the_post(); ?>
						
                          <?php if( get_field('link-vlnd') ): ?> <a href="<?php the_field('link-vlnd'); ?>" target="_blank">
                        <?php else: ?>
                        <a href="<?php the_permalink() ?>">
                         <?php endif; ?>
                          <p class="eventtitle">
						<strong><?php the_title(); ?>, </strong><?php if( get_field('eventstart') ): ?> <?php the_field('eventstart'); ?> - <?php endif; ?><?php the_field('eventdate'); ?>
						</p>
						<p class="eventinfo"><?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?><?php if( get_field('price-event') ): ?><?php the_field('price-event'); ?><?php endif; ?></p></a>
				<?php endwhile; wp_reset_query(); ?>
                <h2><a href="http://valand.se/kalender/">Se alla event på Valand</a></h2>
			