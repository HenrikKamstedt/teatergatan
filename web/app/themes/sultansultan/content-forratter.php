				    <h2 class="copper">
				    <div class="lines">
				    <object class="left-lines" type="image/svg+xml" data="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/3-lines.svg">Placeholder</object>
				    </div>
				    <?php the_field('foratter-rubrik'); ?>
				    				    <div class="lines">
				    <object class="left-lines flip-h" type="image/svg+xml" data="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/3-lines.svg">Placeholder</object>
				    </div>
				    </h2>
				    <div class="information"><h5 class="beige"><italic><?php the_field('foratter-information'); ?></italic></h5></div>
				    <div class="forratter-ratter">
                    <?php if( have_rows('forratter-ratter') ): ?>
                    <?php while( have_rows('forratter-ratter') ): the_row(); ?>   
				            <div class="ratt">
                                <h4 class="left"><?php the_sub_field('dish'); ?>
                                <?php if( get_sub_field('veg') ): ?>
                                <div class="veg"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/veg.svg">
                                </div>
                                <?php endif; ?>
                               </h4>
                                <h4 class="right"><?php the_sub_field('pris'); ?></h4>    
                            </div>
                            <div class="info"><h5 class="beige"><?php the_sub_field('info'); ?></h5></div>
                   	<?php endwhile; ?>
                    <?php endif; ?>
				    </div>