<div class="col-md-8 varmratter">
                 <h2 class="copper">
                    <div class="lines">
				    <object class="left-lines" type="image/svg+xml" data="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/3-lines.svg">Placeholder</object>
				    </div>
                       <?php the_field('varmratter-rubrik'); ?>
                    <div class="lines">
				    <object class="left-lines flip-h" type="image/svg+xml" data="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/3-lines.svg">Placeholder</object>
				    </div>
                </h2>
                       <div class="row">
                        <div class="col-md-6">
          <?php $i = 0; $j = count( get_field('varmratter-ratter') );?>
          <?php if( have_rows('varmratter-ratter') ): while ( have_rows('varmratter-ratter') ) : the_row(); ?>
                            <div class="ratt">
                                <h4 class="left"><?php the_sub_field('dish'); ?>
                                <?php if( get_sub_field('veg') ): ?>
                                <div class="veg"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/veg.svg"></div>
                                <?php endif; ?>
                                </h4>
                                <h4 class="right"><?php the_sub_field('pris'); ?></h4>
                            </div>
                            <div class="info"><h5 class="beige"><?php the_sub_field('info'); ?></h5></div>
            <?php if ( ( $i + 1 ) == ceil($j / 2) ) echo '</div><div class="col-md-6">'; ?>
            <?php $i++; endwhile; ?>
          <?php endif; ?>
        </div>
                    </div>
				    </div>
				<div class="col-md-4 desserter">
                				    <h3 class="deli"><?php the_field('dessert-rubrik'); ?></h3>

                        <?php if( have_rows('dessert') ):
                            while( have_rows('dessert') ): the_row();
                            ?>
                               <div class="dessert">
                                <h4><?php the_sub_field('dish'); ?></h4>
				                <h5 class="beige"><italic><?php the_sub_field('info'); ?></italic></h5>
                        <h4><?php the_sub_field('pris'); ?></h4>
                            </div>
	<?php endwhile; ?>
<?php endif; ?>

                </div>
