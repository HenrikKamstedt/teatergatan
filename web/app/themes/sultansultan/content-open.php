<div class="row">
    <div class="col-xs-6 left small-frame">
      <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/open-top-l.svg">
    </div>
    <div class="col-xs-6 right small-frame">
      <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/open-top-r.svg">
    </div>
</div>

  <h3 class="light-beige open">Öppet</h3>
  <?php if( have_rows('oppettider', 7) ): ?>
  <?php while( have_rows('oppettider', 7) ): the_row(); ?>
   <h4><?php the_sub_field('dag'); ?> <?php the_sub_field('tid'); ?></h4>
<?php endwhile; ?>
<?php endif; ?>
<div class="row">
  <div class="col-xs-6 left small-frame">
    <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/open-btm-l.svg">
  </div>
  <div class="col-xs-6 right small-frame">
    <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/open-btm-r.svg">
  </div>
</div>
