<section id="rooms">
  <div class="container">
    <div class="row">
      <div class="col-md-4 centered">
        <div class="room" data-aos="fade-up">
        <?php
                   $image = get_field('matsalenbild', 7);
                   $size = 'full'; // (thumbnail, medium, large, full or custom size)

               if( $image ) {

            echo wp_get_attachment_image( $image, $size );

               }
               ?>
               <p><strong><?php the_field('matsalenrubrik', 7); ?></strong></p>
               <p class="small"><?php the_field('matsaleninfo', 7); ?></p>
          </div><!-- room -->
    </div><!-- col -->
    <div class="col-md-4 centered">
      <div class="room" data-aos="fade-up">
      <?php
                 $image = get_field('saluhallenbild', 7);
                 $size = 'full'; // (thumbnail, medium, large, full or custom size)

             if( $image ) {

          echo wp_get_attachment_image( $image, $size );

             }
             ?>
             <p><strong><?php the_field('saluhallenrubrik', 7); ?></strong></p>
             <p class="small"><?php the_field('saluhalleninfo', 7); ?></p>
        </div><!-- room -->
  </div><!-- col -->
  <div class="col-md-4 centered">
    <div class="room" data-aos="fade-up">
    <?php
               $image = get_field('barenbild', 7);
               $size = 'full'; // (thumbnail, medium, large, full or custom size)

           if( $image ) {

        echo wp_get_attachment_image( $image, $size );

           }
           ?>
           <p><strong><?php the_field('barenrubrik', 7); ?></strong></p>
           <p class="small"><?php the_field('bareninfo', 7); ?></p>
      </div><!-- room -->
</div><!-- col -->
  </div><!-- row -->
</div>
</section>
