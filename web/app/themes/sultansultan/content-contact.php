<section id="contact">
		<div class="container">
			<div class="row">
				<div class="col-sm-4 openhours">
				<h3>Öppet</h3>
    <?php if( have_rows('oppettider', 7) ): ?>
    <?php while( have_rows('oppettider', 7) ): the_row(); ?>
     <h4><?php the_sub_field('dag'); ?> <?php the_sub_field('tid'); ?></h4>
	<?php endwhile; ?>
<?php endif; ?>
</div><!-- end col -->

                <div class="col-sm-4" data-aos="fade-up">
                    <object type="image/svg+xml" data="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/Teatergatan-logo-full.svg"></object>
				</div><!-- end col -->

                <div class="col-sm-4 contactinfo">
				<h3>Kontakt</h3>
				<h4><?php the_field('kontakt-adress', 307); ?></h4>
				<h4><a href="mailto: <?php the_field('kontakt-mejl', 307); ?>"><?php the_field('kontakt-mejl', 307); ?></a></h4>
				<h4><?php the_field('kontakt-telefon', 307); ?></h4>


                    <div class="social-badge">
                        <a href="<?php the_field('kontakt-facebook', 307); ?>" target="_blank" class="svg">
                        <object class="facebook" type="image/svg+xml" data="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/Facebook-symbol.svg"></object>
                        </a>
                    </div>


                    <div class="social-badge">
                    <a href="<?php the_field('kontakt-instagram', 307); ?>" target="_blank" class="svg">
                        <object class="instagram" type="image/svg+xml" data="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/Instagram-symbol.svg"></object>
                    </a>
                    </div>

				</div><!-- end col -->

			</div><!-- row -->
		</div><!-- container -->
</section><!-- contact -->
