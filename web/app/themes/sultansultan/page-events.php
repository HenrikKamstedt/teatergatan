<?php
/*
	Template Name: Eventflöde
 */

get_header(); ?>

<!-- HERO
    ================================================== -->
    
    	<section id="head">
		<div class="container">
			<div class="row">
            <div class="col-sm-12 centered">
				    <h1 class="page-title"><?php the_title(); ?></h1>
				</div>
                <div class="grid">

			<?php 
	$today = date('Ymd');

                    $args = array (
    					'post_type' 	=> 'event',
    					'meta_key'		=> 'eventdate',
    					'orderby' 		=> 'meta_value',
						'order' 		=> 'ASC',
						'posts_per_page' => -1,
    					'meta_query' 	=> array(
	        				'key'		=> 'eventdate',
	        				'compare'	=> '>=',
	        				'value'		=> $today,
						),
					);

// get posts
$loop = new WP_Query( $args );
?>
				<?php while( $loop->have_posts() ) : $loop->the_post(); ?>
				<div class="grid-sizer grid-item col-md-4">
				            <?php if( get_field('link-vlnd') ): ?> 
                                <a href="<?php the_field('link-vlnd'); ?>" target="_blank">
                            <?php else: ?>
                                <a href="<?php the_permalink() ?>">
                             <?php endif; ?>
				    
				    <div class="shadowbox" style="background-image: url('<?php the_post_thumbnail_url('large'); ?>');
   background-color: #ba8d78;">
					                <div class="grid-item-content centered">
                        <p class="eventinfo"><?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?></p>
                         <h2><?php the_title(); ?></h2>
                          <p class="eventinfo"><?php if( get_field('eventstart') ): ?> <?php the_field('eventstart'); ?> - <?php endif; ?><?php the_field('eventdate'); ?>
						</p>
					        </div><!-- end grid-item-content -->
				                
				    </div>
				    </a>
				</div><!-- end col / grid-item -->
				<?php endwhile; wp_reset_query(); ?>	
			</div><!-- end grid -->

			</div><!-- row -->
		</div><!-- container -->
	</section><!-- info-->
    


<?php
get_footer();
