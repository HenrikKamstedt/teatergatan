<div class="row light-bg">
     <div class="col-md-6 col-md-push-6 no-gutter img-holder flex" style="background-image: url('<?php the_field('dryckbild', 7); ?>')" data-type="background" data-speed="0">
      </div>
      <div class="col-md-6 col-md-pull-6">
        <div class="food-box" data-aos="fade-up">
          <h3 class="black"><?php the_field('dryckrubrik', 7); ?></h3>
          <p class="black md-txt"><?php the_field('dryckinfo', 7); ?></p>
        </div>
      </div>
</div><!-- row -->
