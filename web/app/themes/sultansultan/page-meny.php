<?php
/*
	Template Name: Mat & Dryck
 */

get_header(); ?>

<!-- Meny
================================================== -->
<section id="featureimg-dynamic" style="background-image: url('<?php the_post_thumbnail_url('full'); ?>');">
<div class="container">
  <div class="row">
    <div class="col-md-6 col-md-offset-1 infobox">
      <p class="md-txt"><?php the_field('matinfo', 7); ?></p>
      <a class="onlight" href="#myModal" data-toggle="modal" data-target="#myModal">Boka bord</a> <a class="onlight" href="#menu">Se vår meny</a>
    </div>
    <div class="col-md-3 col-md-offset-1 centered">
      <?php get_template_part( 'content', 'open' ); ?>
    </div>
  </div>

</div>
</section>

<div id="menu">
    <section class="layered">
                    <div class="container-fluid layer-bottom">
        <div class="row centered">
    <div class="col-md-6 col-lg-4 col-lg-offset-2">
                  <div class="oyster-bg rellax" data-rellax-speed="3"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/Oysters.svg"></div>
    </div>
    <div class="col-md-6">
                <div class="artichoke-bg rellax" data-rellax-speed="-1"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/artichoke.svg">
                </div>
            </div>
  </div><!-- row -->
    </div><!-- container -->
    <div class="container layer-top">
        <div class="row centered">
    <div class="col-md-6 delamer" data-aos="fade-right">
                  <?php get_template_part( 'content', 'delamer' ); ?>
    </div>
    <div class="col-md-6 foratter" data-aos="fade-top" data-aos-delay="300">
                <?php get_template_part( 'content', 'forratter' ); ?>
            </div>
  </div><!-- row -->
    </div><!-- container -->
    </section>

    <section class="no-layer deli-outer" data-aos="fade-top">
    <div class="container-fluid">
        <?php get_template_part( 'content', 'deli' ); ?>
    </div><!-- container -->
    </section>

    <section class="layered">
    <div class="container-fluid layer-bottom">
        <div class="row right">
    <div class="col-md-4 col-md-offset-6">
    </div>
  </div><!-- row -->
    </div><!-- container -->
    <div class="container layer-top">
  <div class="row centered chefs-n2t">
        <?php get_template_part( 'content', 'n2t' ); ?>
  </div><!-- row -->
         </div><!-- container -->
</section>

<section class="layered">
                                  <div class="container-fluid layer-bottom">
    <div class="row centered">
      <div class="col-md-5 no-gutter">
                  <div class="cow-bg left rellax" data-rellax-speed="-1"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/Cow.svg"></div>
      </div>
      <div class="col-md-2 col-md-offset-4">
                <div class="spoon-bg rellax" data-rellax-speed="1"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/Spoon.svg">
                </div>
            </div>
          </div><!-- row -->
    </div><!-- container -->

    <div class="container layer-top">
      <div class="row centered" data-aos="fade-top">
          <?php get_template_part( 'content', 'varmratter' ); ?>
      </div><!-- row -->
    </div><!-- container -->

    <div class="container">
        <div class="row centered">
            <div class="col-sm-12">
              <div class="btm-frame">
                 <object class="down-arrow" type="image/svg+xml" data="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/btm-frame.svg"></object>
              </div>
            </div>
        </div>
    </div>
</section><!-- info-->
</div>

<div id="food">
      <div class="container-fluid">
         <?php get_template_part( 'content', 'food' ); ?>
         <?php get_template_part( 'content', 'drinks' ); ?>
      </div><!-- container -->
      <?php get_template_part( 'content', 'rooms' ); ?>
</div><!-- info-->

<?php
get_footer();
