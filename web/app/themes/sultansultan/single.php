<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Sultan_&_Sultan
 */

get_header(); ?>

    <?php while ( have_posts() ) : the_post(); ?>
<?php if ( has_post_thumbnail() ) : ?>
    <section id="featureimg" style="background-image: url('<?php the_post_thumbnail_url('full'); ?>');">
      <div class="container">
        <div class="row" data-aos="fade-up">
            <div class="col-md-5">
                <p class="single category"><?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?></p>
                <h1><?php the_title(); ?></h1>
                <p><?php if( get_field('shortinfo') ): ?> <?php the_field('shortinfo'); ?><?php endif; ?></p>
            </div>
        </div>
      </div>
    </section>
<?php else: ?>
    <section id="head">
    <div class="container">
        <div class="row" data-aos="fade-up">
            <div class="col-sm-12">
                <h1 class="page-title"><?php the_title(); ?></h1>
            </div>
        </div>
    </div>
    </section>

<?php endif; ?>

    <!-- Info
	================================================== -->
	<section id="main">
		<div class="container">
			<div class="row" data-aos="fade-up">
				<div class="col-sm-8">
				    <?php the_content(); ?>
				</div>

<div class="col-sm-3 col-sm-offset-1">
              <div class="sidebarbox centered">
      	         <div class="boka">
		                 <a data-target="#myModal" data-toggle="modal">Boka bord</a>
	                  </div>
                    <?php if( get_field('booking-info') ): ?>
                            <p class="small"><?php the_field('booking-info'); ?></p>
                    <?php endif; ?>
               </div>


               <?php if( get_field('price-event') ): ?>
                   <div class="sidebarbox centered">
                       <h3>Pris</h3>
                       <p><?php the_field('price-event'); ?></p>
                   </div>
               <?php endif; ?>

                     <?php if( get_field('time-event') ): ?>
                   <div class="sidebarbox centered">
                       <h3>Tid</h3>
                       <p><?php the_field('time-event'); ?></p>
                   </div>
               <?php endif; ?>

               <?php if( get_field('place-event') ): ?>
             <div class="sidebarbox centered">
                 <h3>Plats</h3>
                 <p><?php the_field('place-event'); ?></p>
             </div>
         <?php endif; ?>

    <div class="sidebarbox centered">
      <?php get_template_part( 'content', 'open' ); ?>
    </div>

</div>
			</div><!-- row -->
		</div><!-- container -->
	</section><!-- info-->
  <div id="food">
        <div class="container-fluid">
           <?php get_template_part( 'content', 'food' ); ?>
           <?php get_template_part( 'content', 'drinks' ); ?>
        </div><!-- container -->
  </div><!-- info-->


<?php endwhile; ?>

<?php
get_footer();
