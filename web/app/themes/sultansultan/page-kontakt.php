<?php
/*
	Template Name: Kontaktsida
 */

get_header(); ?>

<!-- HERO -->
<section id="featureimg" style="background-image: url('<?php the_post_thumbnail_url('full'); ?>');">
</section>

<section id="contact-page">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-1">
        <div class="row">
          <h3 class="copper"><?php the_field('kontakt-rubrik'); ?></h3>
          <div class="col-sm-7 no-gutter">
            <div class="contact-box">
              <p><?php the_field('kontakt-adress'); ?></p>
              <p>En del av <a href="http://valand.se/" target="_blank">Valandhuset</a><br> </p>
            </div>
            <div class="contact-box">
              <p><a href="mailto: <?php the_field('kontakt-mejl'); ?>"><?php the_field('kontakt-mejl'); ?></a></p>
              <p><?php the_field('kontakt-telefon'); ?></p>
            </div>
          </div>
          <div class="col-sm-5 no-gutter centered">
            <div class="booking-box">
              <p><a href="#myModal" data-toggle="modal" data-target="#myModal">Boka bord</a></p>
            </div>
            <div class="contact-box">
              <p><a href="<?php the_field('kontakt-facebook'); ?>">Facebook</a></p>
              <p><a href="<?php the_field('kontakt-instagram'); ?>">Instagram</a></p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-md-offset-1 centered">
        <?php get_template_part( 'content', 'open' ); ?>
      </div>
    </div>
    <div class="row jobs">
      <?php $loop = new WP_Query( array(
        'cat'			        =>	6,
        'orderby'			    =>	'date',
        'order'				    =>	'DESC',
        'posts_per_page'	=>	'-1',
    ) ) ?>
  <?php while( $loop->have_posts() ) : $loop->the_post(); ?>
    <div class="col-md-6 centered">
      <div class="event" style="background-image: url('<?php the_post_thumbnail_url( 'large' ); ?>')">
      <a href="<?php the_permalink() ?>" class="event-content">
      <p class="category"><?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?></p>
      <h3><?php the_title(); ?></h3>
      <p class="eventinfo"><?php the_field('shortinfo'); ?></p>
      </a><!-- event-content -->
      </div><!-- event -->
    </div>
  <?php endwhile; wp_reset_query(); ?>
    </div>
  </div>
</section>

<?php
get_footer();
