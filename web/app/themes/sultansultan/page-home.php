<?php
/*
	Template Name: Landningssida
 */

get_header(); ?>

<!-- HERO
    ================================================== -->

    <section id="feature" class="flex" style="background-image: url('<?php the_field('tgbg'); ?>')" data-type="background" data-speed="-2">
      <!-- Play hero-background-video when loaded. When not loaded show background picture defined in earlier stage. -->
      <video autoplay muted loop id="bg-video">
        <!-- Fetch MP4 -->
        <source src="<?php

          $value = the_field('tgbg-video-mp4');

          if( $value ) {
            echo $value;
          } else {
            echo null;
          }

        ?>" type="video/mp4">
        <!-- Fetch WEBM -->
        <source src="<?php

          $value = the_field('tgbg-video-webm');

          if( $value ) {
            echo $value;
          } else {
            echo null;
          }

        ?>" type="video/webm">
      </video>
    <div class="tgsymbol" data-aos="fade-up">
               <?php
                            $image = get_field('tgsymbol');
                            $size = 'full'; // (thumbnail, medium, large, full or custom size)

                        if( $image ) {

                     echo wp_get_attachment_image( $image, $size );

                        }

                        ?>
           </div>
         <div class="down">
                    <a href="#info" class="svg">
                     <object class="down-arrow" type="image/svg+xml" data="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/down-arrow.svg"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/down-arrow.svg" alt="Arrow"></object>
                     </a>
         </div>
    </section>


    <!-- OM TEATERGATAN
	================================================== -->
	<section id="info">
		<div class="container">
			<div class="row" data-aos="fade-up">
				<div class="col-md-9">
				    <h1 class="copper"><?php the_field('inforubrik'); ?></h1>
				    <p><?php the_field('infobrodtext'); ?></p>
				</div>
        <div class="col-md-3 centered">
          <?php get_template_part( 'content', 'open' ); ?>
        </div>
			</div><!-- row -->
		</div><!-- container -->
	</section><!-- info-->

    <!-- MAT & DRYCK
	================================================== -->
	<div id="food">
        <div class="container-fluid">
	         <?php get_template_part( 'content', 'food' ); ?>
           <?php get_template_part( 'content', 'drinks' ); ?>
        </div><!-- container -->
	</div><!-- info-->

           <?php get_template_part( 'content', 'rooms' ); ?>

    <!-- NYHETER & EVENT
	================================================== -->
	<section id="nyheter">
		<div class="container">
			<div class="row">
        <div class="col-sm-12 centered">
          <h1 class="copper"><?php the_field('news-rubrik'); ?></h1>
        </div>
				<div class="col-md-6 centered">
          <?php
            $post_object = get_field('push1');

if( $post_object ):

	// override $post
	$post = $post_object;
	setup_postdata( $post );

	?>
    <div class="event" style="background-image: url('<?php the_post_thumbnail_url( 'large' ); ?>')">
    <a href="<?php the_permalink() ?>" class="event-content">
    <p class="category"><?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?></p>
    <h3><?php the_title(); ?></h3>
    <p class="eventinfo"><?php the_field('shortinfo'); ?></p>
    </a><!-- event-content -->
    </div><!-- event -->
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>

        </div>
        <div class="col-md-6 centered">
          <?php
            $post_object = get_field('push2');

if( $post_object ):

  // override $post
  $post = $post_object;
  setup_postdata( $post );

  ?>
    <div class="event" style="background-image: url('<?php the_post_thumbnail_url( 'full' ); ?>')">
    <a href="<?php the_permalink() ?>" class="event-content">
    <p class="category"><?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?></p>
    <h3><?php the_title(); ?></h3>
    <p class="eventinfo"><?php the_field('shortinfo'); ?></p>
    </a><!-- event-content -->
    </div><!-- event -->
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>

        </div>
        <div class="col-md-6 centered">
          <?php
            $post_object = get_field('push3');

if( $post_object ):

  // override $post
  $post = $post_object;
  setup_postdata( $post );

  ?>
    <div class="event" style="background-image: url('<?php the_post_thumbnail_url( 'large' ); ?>')">
    <a href="<?php the_permalink() ?>" class="event-content">
    <p class="category"><?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?></p>
    <h3><?php the_title(); ?></h3>
    <p class="eventinfo"><?php the_field('shortinfo'); ?></p>
    </a><!-- event-content -->
    </div><!-- event -->
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>

        </div>
        <div class="col-md-6 centered">
          <?php
            $post_object = get_field('push4');

if( $post_object ):

  // override $post
  $post = $post_object;
  setup_postdata( $post );

  ?>
    <div class="event" style="background-image: url('<?php the_post_thumbnail_url( 'full' ); ?>')">
    <a href="<?php the_permalink() ?>" class="event-content">
    <p class="category"><?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?></p>
    <h3><?php the_title(); ?></h3>
    <p class="eventinfo"><?php the_field('shortinfo'); ?></p>
    </a><!-- event-content -->
    </div><!-- event -->
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>

        </div>
        <div class="col-sm-12 centered all-event">
          <a class="onlight" href="/nyheter-event">Se alla våra event</a>
        </div>
			</div><!-- row -->
		</div><!-- container -->
	</section><!-- nyheter -->

<?php
get_footer();
