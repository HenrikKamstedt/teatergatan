<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Sultan_&_Sultan
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

<!-- Google Analytics -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-110075294-1', 'auto');
ga('send', 'pageview');
</script>
<!-- End Google Analytics -->

<meta name="google-site-verification" content="t8jzNNTkWdViI7htt16KRgkRy-BzzmTM-Hm5MEtHdV0" />
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<!-- Bootstrap core CSS -->
<link href="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/css/aos.css" rel="stylesheet">


<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,600|Playfair+Display:400,400i,700" rel="stylesheet">

<?php wp_head(); ?>

	<!-- HTML5 shiv and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'sultansultan' ); ?></a>

		<!-- HEADER
	================================================== -->
	<header class="site-header" role="banner">

		<!-- NAVBAR
		================================================== -->
		<div class="navbar-wrapper">

			<div class="navbar navbar-fixed-top" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only">Visa meny</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<div class="logo">
						<a class="navbar-brand" href="/">
<object type="image/svg+xml" data="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/Teatergatan_logo_txt.svg"></object></a>
						</div>
					</div>

					<div class="button_container" id="toggle"><span class="top"></span><span class="middle"></span><span class="bottom"></span></div>
    <div class="overlay" id="overlay">
      <nav class="overlay-menu">
<?php
						wp_nav_menu( array(

						'theme_location'	=>	'menu-l',
						'container'			=>	'div',
						'container_class'	=>	'left-nav',
						'menu_class'		=>	'nav navbar-nav'
						) );
					?>
										<?php
						wp_nav_menu( array(

						'theme_location'	=>	'menu-r',
						'container'			=>	'div',
						'container_class'	=>	'right-nav',
						'menu_class'		=>	'nav navbar-nav'
						) );
					?>
      </nav>
    </div>

                    <nav class="navbar-collapse collapse">
					<?php
						wp_nav_menu( array(

						'theme_location'	=>	'menu-l',
						'container'			=>	'div',
						'container_class'	=>	'left-nav',
						'menu_class'		=>	'nav navbar-nav'
						) );
					?>
										<?php
						wp_nav_menu( array(

						'theme_location'	=>	'menu-r',
						'container'			=>	'div',
						'container_class'	=>	'right-nav',
						'menu_class'		=>	'nav navbar-nav'
						) );
					?>
					</nav>
			</div> <!-- navbar-->

		</div> <!-- navbar wrapper -->
	</header>
