<div class="row light-bg">
     <div class="col-md-6 no-gutter img-holder flex" style="background-image: url('<?php the_field('matbild', 7); ?>')" data-type="background" data-speed="0">
     </div>
     <div class="col-md-6">
       <div class="food-box" data-aos="fade-up">
         <h3 class="black"><?php the_field('matrubrik', 7); ?></h3>
         <p class="black md-txt"><?php the_field('matinfo', 7); ?></p>
         <a class="onlight" href="/mat-dryck">Se vår meny</a> <a class="onlight" href="#myModal" data-toggle="modal" data-target="#myModal">Boka bord</a>
       </div>
     </div>
</div><!-- row -->
