	<!-- FOOTER LOGOS
	================================================== -->
	<section id="footer-logos">
		<div class="container-fluid">
			<div class="row">
               <div class="col-md-6">
                   <p class="small text-left foot-text">
                       Teatergatan är en del av <a href="http://valand.se/">Valandhuset</a>
                   </p>
               </div>
                <div class="col-md-6 align-right">
										<div class="footlogo">
											<a href="http://valand.se/" class="svg">
												<object class="valand" type="image/svg+xml" data="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/valand-logo-full.svg"></object>
											</a>
										</div>

                </div>
			</div><!-- row -->
		</div><!-- container -->
	</section>
