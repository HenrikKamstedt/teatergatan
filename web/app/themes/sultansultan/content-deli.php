        <div class="row">
            <div class="col-xs-6 left frame">
			    <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/chark-top-l.svg">
			</div>
                   <div class="col-xs-6 right frame">
			    <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/chark-top-r.svg">
			</div>
        </div>
			<div class="row centered delikatesser" style="background-image: url('<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/chark-bg.svg')">

                <div class="col-md-4 col-sm-12 single-deli">

                        <?php if( have_rows('charketurier') ):
                            while( have_rows('charketurier') ): the_row();
                            ?>
                                <h3 class="deli"><?php the_sub_field('rubrik'); ?></h3>
                    <div class="deli">
                    <div class="deli-dish">
                    <?php if( have_rows('ratter') ): ?>
                    <?php while( have_rows('ratter') ): the_row(); ?>
                            <h4><?php the_sub_field('dish'); ?></h4>
                            <h5><?php the_sub_field('info'); ?></h5>
                            <h4><?php the_sub_field('pris'); ?></h4>
	                <?php endwhile; ?>
                    <?php endif; ?>
                     </div>
                     <h5 class="deli-info"><italic><?php the_sub_field('info'); ?></italic></h5>
               	<?php endwhile; ?>
                <?php endif; ?>
                </div>
                </div>

                <div class="col-md-4 col-sm-12 single-deli">

                          <?php if( have_rows('utvalda-kok') ):
                              while( have_rows('utvalda-kok') ): the_row();
                              ?>
                                  <h3 class="deli"><?php the_sub_field('rubrik'); ?></h3>
                                  <h5><italic><?php the_sub_field('info'); ?></italic></h5>
                      <div class="deli favoriter">
                      <?php if( have_rows('ratter') ): ?>
                      <?php while( have_rows('ratter') ): the_row(); ?>
                          <div class="deli-dish">
                              <h5><?php the_sub_field('dish'); ?></h5> <h4><?php the_sub_field('pris'); ?></h4>
                          </div>

  	                <?php endwhile; ?>
                      <?php endif; ?>
                 	<?php endwhile; ?>
                  <?php endif; ?>
                  </div>
                  </div>

                  <div class="col-md-4 col-sm-12 single-deli">

                          <?php if( have_rows('ostar') ):
                              while( have_rows('ostar') ): the_row();
                              ?>
                                  <h3 class="deli"><?php the_sub_field('rubrik'); ?></h3>
                      <div class="deli">
                      <div class="deli-dish">
                        <?php if( have_rows('ratter') ): ?>
                        <?php while( have_rows('ratter') ): the_row(); ?>
                                <h4><?php the_sub_field('dish'); ?></h4>
                                <h5><?php the_sub_field('info'); ?></h5>
                                <h4><?php the_sub_field('pris'); ?></h4>
    	                <?php endwhile; ?>
                        <?php endif; ?>
                         </div>
                         <h5 class="deli-info"><italic><?php the_sub_field('info'); ?></italic></h5>
                 	<?php endwhile; ?>
                  <?php endif; ?>
                  </div>
                  </div>

                </div><!-- row -->
                        <div class="row">
            <div class="col-xs-6 left frame">
			    <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/chark-btm-l.svg">
			</div>
                   <div class="col-xs-6 right frame">
			    <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/chark-btm-r.svg">
			</div>
        </div>
