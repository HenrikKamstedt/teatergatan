<div class="col-sm-3 chefs-choice"  data-aos="fade-right">
                   <?php if( have_rows('chefs-vegetarian') ): ?>
                    <?php while( have_rows('chefs-vegetarian') ): the_row(); ?>
                        <div class="chefs"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/Chefs.svg" alt="Chef's"></div>
                        <div class="choice">
                                <h3><?php the_sub_field('rubrik'); ?> <div class="veg"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/veg.svg"></div>
</h3>
				                <p class="beige"><?php the_sub_field('info'); ?></p>
                                <h4><?php the_sub_field('pris'); ?></h4>
                        </div>
	                <?php endwhile; ?>
                    <?php endif; ?>
                    </div>
				<div class="col-sm-6" data-aos="fade">
                   <?php if( have_rows('nose2tail') ): ?>
                    <?php while( have_rows('nose2tail') ): the_row(); ?>
                        <div class="nose2tail">
                               <div class="row">
                                   <div class="col-xs-3">
                                       <h4><?php the_sub_field('antal'); ?></h4>
                                   </div>
                                   <div class="col-xs-6">
                                    <div class="n2t"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/nose2tail.svg" alt="Nose to Tail"></div>
                                   </div>
                                   <div class="col-xs-3">
                                       <h4><?php the_sub_field('pris'); ?></h4>
                                   </div>
                               </div>
                            <h3><?php the_sub_field('info'); ?></h3>
                        </div>
	                <?php endwhile; ?>
                    <?php endif; ?></div>
				<div class="col-sm-3 chefs-choice" data-aos="fade-left">
                                   <?php if( have_rows('chefs-choice') ): ?>
                    <?php while( have_rows('chefs-choice') ): the_row(); ?>
                        <div class="chefs"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/Chefs.svg" alt="Chef's"></div>
                        <div class="choice">
                                <h3><?php the_sub_field('rubrik'); ?></h3>
				                <p class="beige"><?php the_sub_field('info'); ?></p>
                                <h4><?php the_sub_field('pris'); ?></h4>
                        </div>
	                <?php endwhile; ?>
                    <?php endif; ?>
                </div>
