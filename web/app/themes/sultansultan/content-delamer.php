       <div class="head"><h2 class="copper"><?php the_field('delamer-rubrik'); ?></h2></div>
                       <div class="delamer-single">
                        <?php if( have_rows('delamer-dish') ):
                            while( have_rows('delamer-dish') ): the_row();
                            ?>
                                <h3><?php the_sub_field('dish'); ?></h3>
				                <h5 class="beige"><?php the_sub_field('info'); ?></h5>
                        <?php if( get_sub_field('extrainfo') ): ?>
                          <h5 class="beige"><italic><?php the_sub_field('extrainfo'); ?></italic></h5>
                        <?php endif; ?>
                                <h4><?php the_sub_field('pris'); ?></h4>
	                        <?php endwhile; ?>
	                    <?php endif; ?>
	                    </div> <!-- single -->
