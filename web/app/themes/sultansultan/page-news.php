<?php
/*
	Template Name: Nyhetsflöde
 */

get_header(); ?>

<!-- HERO
    ================================================== -->

  <section id="news">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 centered">
				    <h1 class="page-title"><?php the_title(); ?></h1>
				</div>

				<!-- Meny -->
				<ul class="nav nav-pills">
					<li class="nav-item">
						<a class="nav-link active" href="/nyheter-event">Alla nyheter & event</a>
					</li>
					<li class="nav-item">
						<a class="nav-link active" href="?filter=8">Erbjudande</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="?filter=6">Lediga jobb</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="?filter=7">Event</a>
					</li>
				</ul>

				<!-- Undermeny -->
				<!-- If 'Live på Valand' display under nav -->
				<ul class="nav nav-pills" style="<?php
							if (array_key_exists("filter", $_GET) === true && $_GET['filter'] == 7) {
									echo 'display: block;';
								} else {
									echo 'display: none;';
								}
							?> font-size: 12px;">
					<li class="nav-item">
						<!-- Create dynamic link for scaleable use -->
						<a class="nav-link active" href="<?php
							if (array_key_exists("filter", $_GET) === true)
								{
									echo '?filter=', $_GET['filter'], '&';
								}
							?>sort=alla">Alla</a>
					</li>
					<li class="nav-item">
						<a class="nav-link active" href="<?php
							if (array_key_exists("filter", $_GET) === true)
								{
									echo '?filter=', $_GET['filter'], '&';
								}
							?>sort=kommande">Kommande</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?php
							if (array_key_exists("filter", $_GET) === true)
								{
									echo '?filter=', $_GET['filter'], '&';
								}
							?>sort=tidigare">Tidigare</a>
					</li>
				</ul>
				<!-- Check if URL and _GET is available otherwise display all -->
				<?php
					if (array_key_exists("filter", $_GET) === true && array_key_exists("sort", $_GET) === true) {
						$sortQuery = sortEvents($_GET['filter'], $_GET['sort']);
					} else if (array_key_exists("filter", $_GET) === true) {
						$sortQuery = sortEvents($_GET['filter'], false);
					} else {
						$sortQuery = sortEvents('null', false);
					}
				?>
				<!-- Check if we have any posts -->
				<?php if ( $sortQuery->have_posts() ) : while( $sortQuery->have_posts() ) : $sortQuery->the_post(); ?>
          <div class="col-md-6 centered">
            <div class="event" style="background-image: url('<?php the_post_thumbnail_url( 'large' ); ?>')">
            <a href="<?php the_permalink() ?>" class="event-content">
            <p class="category"><?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?></p>
            <h3><?php the_title(); ?></h3>
            <p class="eventinfo"><?php the_field('shortinfo'); ?></p>
            </a><!-- event-content -->
            </div><!-- event -->
          </div>
				<?php endwhile; endif; wp_reset_query(); ?>
        <div class="col-sm-12 centered all-event">
          <a class="onlight" href="http://valand.se/kalender/" target="_blank">Se allt som händer på Valand</a>
        </div>
			</div><!-- row -->
		</div><!-- container -->
	</section><!-- info-->



<?php
get_footer();
