<?php
/*
	Template Name: Nyhetsflöde
 */

get_header(); ?>

<!-- HERO
    ================================================== -->

    	<section id="news">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 centered">
				    <h1 class="page-title"><?php the_title(); ?></h1>
				</div>
				    <?php $loop = new WP_Query( array(
					'orderby'			=>	'date',
					'order'				=>	'DESC',
					'posts_per_page'	=>	'-1',
					) ) ?>
				<?php while( $loop->have_posts() ) : $loop->the_post(); ?>
          <div class="col-md-6 centered">
            <div class="event" style="background-image: url('<?php the_post_thumbnail_url( 'large' ); ?>')">
            <a href="<?php the_permalink() ?>" class="event-content">
            <p class="category"><?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?></p>
            <h3><?php the_title(); ?></h3>
            <p class="eventinfo"><?php the_field('shortinfo'); ?></p>
            </a><!-- event-content -->
            </div><!-- event -->
          </div>
				<?php endwhile; wp_reset_query(); ?>
        <div class="col-sm-12 centered all-event">
          <a class="onlight" href="http://valand.se/kalender/" target="_blank">Se allt som händer på Valand</a>
        </div>
			</div><!-- row -->
		</div><!-- container -->
	</section><!-- info-->



<?php
get_footer();
