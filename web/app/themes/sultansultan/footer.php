<?php
/*
	Template Name: Footer
 */
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Sultan_&_Sultan
 */

?>

<?php wp_footer(); ?>

<footer>	
	<?php get_template_part( 'content', 'contact' ); ?>
	<?php get_template_part( 'content', 'footerlogos' ); ?>	

		
	</footer>
 

   <!-- Bootstrap core JavaScript + other
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php bloginfo( 'template_directory' ); ?>/assets/js/jquery-3.2.1.min.js"></script>
    <script src="<?php bloginfo( 'template_directory' ); ?>/assets/js/bootstrap.min.js"></script>  
    <script src="<?php bloginfo( 'template_directory' ); ?>/assets/js/rellax.min.js"></script>
    <script src="<?php bloginfo( 'template_directory' ); ?>/assets/js/aos.js"></script>
    <script src="<?php bloginfo( 'template_directory' ); ?>/assets/js/masonry.pkgd.min.js"></script>
    <script src="<?php bloginfo( 'template_directory' ); ?>/assets/js/main.js"></script>

<?php get_template_part( 'content', 'modal' ); ?>
</body>
</html>
