	    <section class="row" id="primary">
	    <?php $loop = new WP_Query( array(
					'post_type'			=>	'vallokaler',
					'orderby'			=>	'post_id',
					'order'				=>	'ASC',
					) ) ?>
		<?php while( $loop->have_posts() ) : $loop->the_post(); ?>
	    	<div class="col-sm-6">
	    		<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
	    		<div class="lokal" style="background-image: url('<?php the_post_thumbnail_url( 'full' ); ?>')">
	    			<h2 class="text-center"><?php the_title(); ?></h2>
	    		</div><!-- end lokal -->
	    		</a>
	    	</div><!-- end col -->
	    <?php endwhile; wp_reset_query(); ?>
	    </section><!-- primary -->