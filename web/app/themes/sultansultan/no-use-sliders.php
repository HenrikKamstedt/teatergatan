				    <div class="sliders">
				                            <?php if( have_rows('sliders') ): 
                            while( have_rows('sliders') ): the_row(); 		
                            ?>
                                <h4><?php the_sub_field('rubrik'); ?> <?php the_sub_field('pris'); ?></h4>
				               
<div class="slide">
  <div class="slider">
    <?php if( have_rows('sliders-ratter') ): ?>
        <?php while( have_rows('sliders-ratter') ): the_row(); ?>
            <h5 class="beige"><strong><?php the_sub_field('rubrik'); ?></strong> <?php the_sub_field('info'); ?></h5>  
	    <?php endwhile; ?>
    <?php endif; ?>
    </div>
          <div class="tillbehor">
    <?php if( have_rows('tillbehor') ): ?>
        <?php while( have_rows('tillbehor') ): the_row(); ?>
              <div class="oyster">
                <h4 class="left"><?php the_sub_field('rubrik'); ?></h4>
                <h4 class="right"><?php the_sub_field('pris'); ?></h4>  
              </div>
	    <?php endwhile; ?>
    <?php endif; ?>
    </div>
               	                        <?php endwhile; ?>
	                    <?php endif; ?>
				   
				    </div> 
				</div>
				
			
		
	<div class="delamer-single">
       <div class="head"><h2 class="copper"><?php the_field('delamer-rubrik'); ?></h2></div>
                       
        <?php if( have_rows('ostron') ): 
                            while( have_rows('ostron') ): the_row(); 		
                            ?>
                                <div class="knife"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/knife-left.png"></div>
                                <h3><?php the_sub_field('rubrik'); ?></h3>
                                <div class="knife"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/knife-right.png"></div>
				                <p><?php the_sub_field('pris'); ?></p>
<div class="oysters">
   <?php if( have_rows('ostron_styck') ): ?>
    <?php while( have_rows('ostron_styck') ): the_row(); ?>
    <div class="oyster">
        <h4 class="left"><?php the_sub_field('dish'); ?></h4>
        <h4 class="right"><?php the_sub_field('pris'); ?></h4>    
    </div>

	<?php endwhile; ?>
<?php endif; ?>
</div>

				                <h5 class="beige"><italic><?php the_sub_field('info'); ?></italic></h5>
	                        <?php endwhile; ?>
	                    <?php endif; ?>
	                    </div> <!-- single -->
                        
                        <div class="delamer-single">
                         <?php if( have_rows('havskraftor') ): 
                            while( have_rows('havskraftor') ): the_row(); 		
                            ?>
                                <h3><?php the_sub_field('rubrik'); ?></h3>
				                <p><?php the_sub_field('pris'); ?></p>
				                <h5 class="beige"><italic><?php the_sub_field('info'); ?></italic></h5>
	                        <?php endwhile; ?>
	                    <?php endif; ?>
                        </div> <!-- single -->

                       <div class="delamer-single">
                        <?php if( have_rows('hummer') ): 
                            while( have_rows('hummer') ): the_row(); 		
                            ?>
                                <h3><?php the_sub_field('rubrik'); ?></h3>
				                <p><?php the_sub_field('pris'); ?></p>
				                <h5 class="beige"><italic><?php the_sub_field('info'); ?></italic></h5>
	                        <?php endwhile; ?>
	                    <?php endif; ?>
                        </div> <!-- single -->
                                <div class="divider"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/divider.png"></div>
                       <div class="delamer-single">
                        <?php if( have_rows('raksmorgas') ): 
                            while( have_rows('raksmorgas') ): the_row(); 		
                            ?>
                                <h3><?php the_sub_field('rubrik'); ?></h3>
				                <h5 class="beige"><italic><?php the_sub_field('info'); ?></italic></h5>
                                <h4><?php the_sub_field('pris'); ?></h4>
	                        <?php endwhile; ?>
	                    <?php endif; ?>
	                    </div> <!-- single -->