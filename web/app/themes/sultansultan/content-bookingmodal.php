 <?php 
// ACF
$col_img_one		= get_field('col_img_one', 86);
$col_head_one		= get_field('col_head_one', 86);
$col_text_one		= get_field('col_text_one', 86);
$col_link_one		= get_field('col_link_one', 86);

$col_img_two		= get_field('col_img_two', 86);
$col_head_two		= get_field('col_head_two', 86);
$col_text_two		= get_field('col_text_two', 86);
$col_link_two		= get_field('col_link_two', 86);

$col_img_three		= get_field('col_img_three', 86);
$col_head_three		= get_field('col_head_three', 86);
$col_text_three		= get_field('col_text_three', 86);
$col_link_three		= get_field('col_link_three', 86);

$col_img_four		= get_field('col_img_four', 86);
$col_head_four		= get_field('col_head_four', 86);
$col_text_four		= get_field('col_text_four', 86);
$col_link_four		= get_field('col_link_four', 86);

?>

	<!-- TOP FOOTER
	================================================== -->
	<section id="categories">
		<div class="container">
			<div class="row">
			<a href="<?php echo $col_link_one; ?>" class="cat-link">
			<div class="col-md-3 col-sm-6 col-xs-12">
    			<?php if( !empty($col_img_one) ) : ?>
    				<img src="<?php echo $col_img_one['url']; ?>" alt="<?php echo $col_img_one['alt']; ?>">
		    	<?php endif; ?>				
		    	<h2 class="gold"><?php echo $col_head_one; ?></h2>
				<p><?php echo $col_text_one; ?></p>
			</div>
			</a>
			<a href="<?php echo $col_link_two; ?>" class="cat-link">
			<div class="col-md-3 col-sm-6 col-xs-12">
    			<?php if( !empty($col_img_two) ) : ?>
    				<img src="<?php echo $col_img_two['url']; ?>" alt="<?php echo $col_img_two['alt']; ?>">
		    	<?php endif; ?>				
		    	<h2 class="gold"><?php echo $col_head_two; ?></h2>
				<p><?php echo $col_text_two; ?></p>
			</div>
			</a>
			<a href="<?php echo $col_link_three; ?>" class="cat-link">
			<div class="col-md-3 col-sm-6 col-xs-12">
    			<?php if( !empty($col_img_three) ) : ?>
    				<img src="<?php echo $col_img_three['url']; ?>" alt="<?php echo $col_img_three['alt']; ?>">
		    	<?php endif; ?>				
		    	<h2 class="gold"><?php echo $col_head_three; ?></h2>
				<p><?php echo $col_text_three; ?></p>
			</div>
			</a>
			<a href="<?php echo $col_link_four; ?>" class="cat-link">
			<div class="col-md-3 col-sm-6 col-xs-12">
    			<?php if( !empty($col_img_four) ) : ?>
    				<img src="<?php echo $col_img_four['url']; ?>" alt="<?php echo $col_img_four['alt']; ?>">
		    	<?php endif; ?>				
		    	<h2 class="gold"><?php echo $col_head_four; ?></h2>
				<p><?php echo $col_text_four; ?></p>
			</div>
			</a>
			</div><!-- row -->
			<hr>
		</div><!-- container -->
	</section><!-- categories -->